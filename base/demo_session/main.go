package main

import (
	"fmt"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/sessions"
	"net/http"
)

func main() {
	r := gin.Default()

	// secret 参数是用于加密的密钥
	store := cookie.NewStore([]byte("secret"))
	// 设置session中间件,参数mysession, 指的是session的名字, 也是cookie的名字
	// store是前面创建的存储引擎, 我们可以替换成其他的存储引擎
	r.Use(sessions.Sessions("mysession", store))

	r.GET("/hello", func(c *gin.Context) {
		// 初始化session对象
		session := sessions.Default(c)
		// 通过session.Get读取到session的值
		// session是键值对格式的数据,因此需要通过key查询数据
		if session.Get("hello") != "world" {
			fmt.Println("not read value")
			// 设置session数据
			session.Set("hello", "world")
			session.Save()
		}
		c.JSON(http.StatusOK, gin.H{"hello":session.Get("hello")})
	})
	r.Run(":8888")
}
