package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type Person struct {
	Name string `form:"name"`
	Address string	`form:"address"`
	Birthday string	`form:"birthday" time_format:"2006-01-02" time_utc:"1"`
}

func startPage(c *gin.Context) {
	var person Person
	//if c.ShouldBindQuery(&person) == nil {
	if c.ShouldBind(&person) == nil {
		log.Print(person.Name)
		log.Print(person.Address)
		log.Print(person.Birthday)
	}
	c.String(http.StatusOK, "Success")
}

func getPersonFromPost(c *gin.Context)  {
	var person Person
	 err:= c.ShouldBind(&person)
	if err != nil {
		log.Fatal(err.Error())
		return
	}

	_, _ = c.Writer.Write([]byte(person.Name + " " + person.Address + " " + person.Birthday))
}

func bindTestJson(c *gin.Context) {
	var person Person
	if err:= c.ShouldBindJSON(&person); err != nil{
		log.Fatal(err.Error())
		return
	}

	c.Writer.Write([]byte(person.Name + " " + person.Address + " " + person.Birthday))
}

func main() {
	r := gin.Default()
	r.GET("/testing", startPage)
	r.POST("/testing", getPersonFromPost)
	r.POST("/personJson", bindTestJson)
	r.Run(":8888")
}
