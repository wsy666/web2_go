package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)


// []byte切片类型
func returnByteHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		fullPath := c.FullPath()
		c.Writer.Write([]byte("fullPath: " + fullPath))
	}
}


// json 类型
type Stu struct {
	Name string
	Id int
	Extra string
}

func returnJsonHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		fullPath := c.FullPath()
		c.JSON(http.StatusOK, map[string]interface{}{
			"code" : 1,
			"msg": "ok",
			"data": fullPath,
		})
	}
}

// 结构体类型
func returnStructHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		fullPath := c.FullPath()
		var stu Stu
		stu.Name = "zhang3"
		stu.Id = 1
		stu.Extra = fullPath
		c.JSON(http.StatusOK, &stu)
	}
}


func main() {
	r := gin.Default()
	r.GET("/byte", returnByteHandler())
	r.GET("/json", returnJsonHandler())
	r.GET("/struct", returnStructHandler())
	r.Run(":8888")
}