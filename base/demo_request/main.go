package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	r.GET("/baseReq", func(c *gin.Context) {
		// 获取请求的基本信息
		// 获取请求头
		//"reqHeader", c.Request.Header,
		c.JSON(http.StatusOK, gin.H{
			"reqMethod" : c.Request.Method,
			"reqURL" : c.Request.URL,
			"reqRemoteAddr" : c.Request.RemoteAddr,
			"ClientIp" : c.ClientIP(),
			//"HeadAgent", c.Request.Header["User-Agent"],
			//"HeadAgent2", c.GetHeader("User-Agent"),
		})

	})


	/*
	GET 请求参数
	*/

	r.GET("baseQuery", func(c *gin.Context) {
		name := c.Query("name")
		name2 := c.DefaultQuery("name", "defaultName")
		name3, ok := c.GetQuery("name")
		var okStr string
		if ok {
			okStr = "true"
		} else {
			okStr = "false"
		}
		c.JSON(http.StatusOK, gin.H{
			"name-Query":        name,
			"name-DefaultQuery": name2,
			"name-GetQuery":     name3,
			"ok ":               okStr,
		})
	})

	/*
	获取URL路径参数
	*/
	r.GET("/user/:id", func(c *gin.Context) {
		id := c.Param("id")
		c.JSON(200, gin.H{
			"id " : string(id),
			"idstring": id,
		})

	})



	r.Run(":8888")
}
