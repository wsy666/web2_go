package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)
/*
请求参数  普通参数
*/
func main() {
	r := gin.Default()
	r.GET("/user/save", func(c *gin.Context) {
		id := c.Query("id")
		name := c.Query("name")
		address := c.DefaultQuery("address", "beijing")
		c.JSON(http.StatusOK, gin.H{
			"id":id,
			"address":address,
			"name":name,
		})

	})

	// 判断参数是否存在
	r.GET("/user/save1", func(c *gin.Context) {
		id, ok := c.GetQuery("id")
		address, aok := c.GetQuery("address")
		c.JSON(http.StatusOK, gin.H{
			"id":id,
			"idok":ok,
			"address":address,
			"aok":aok,
		})

	})

	// 上面的id都是string类型,根据类型获取

	type User struct {
		Id int64 `form:"id"`
		Name string `form:"name"`
	}

	r.GET("/user/int", func(c *gin.Context) {
		var user User
		err := c.BindQuery(&user)
		if err != nil {
			fmt.Println(err)
		}
		c.JSON(200, user)

	})

	// 也可以
	// 区别
	// 当binding 标签是必须的时候，ShouldBindQuery会报错，开发者自行处理，状态码不变。

	r.GET("/user/int2", func(c *gin.Context) {
		var user User
		err := c.ShouldBindQuery(&user)
		if err != nil {
			fmt.Println(err)
		}
		c.JSON(200, user)

	})


	// 表单参数
	r.POST("/user/save", func(c *gin.Context) {
		id := c.PostForm("id")
		name := c.PostForm("name")
		address := c.PostFormArray("address")
		addressMap := c.PostFormMap("addressMap")
		c.JSON(http.StatusOK, gin.H{
			"id"	: id,
			"name"	: name,
			"address"	: address,
			"addressMap"	: addressMap,
		})

	})


	r.POST("/user/save1", func(c *gin.Context) {
		var user User
		err := c.ShouldBind(&user)
		addressMap, _ := c.GetPostFormMap("addressMap")
		user.A

	})

	r.Run(":8888")
}
