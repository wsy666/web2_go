package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

//路由组

//
func main() {
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		v1.POST("/login", loginEndpoint)
		v1.POST("/submit", submitEndpoint)
		v1.POST("/read", readEndpoint)
	}

	v2 := r.Group("/v2")
	{
		v2.POST("/login", loginEndpoint)
		v2.POST("/submit", submitEndpoint)
		v2.POST("read", readEndpoint)
	}

	r.Run(":8888")
}

func loginEndpoint(ctx *gin.Context)  {
	fmt.Println("login: ", ctx.Request.URL.Path)
	ctx.JSON(http.StatusOK, gin.H{
		"message":"login",
		"path":ctx.Request.URL.Path,
	})
}

func submitEndpoint(ctx *gin.Context)  {
	fmt.Println("submit", ctx.Request.URL.Path)
	ctx.JSON(http.StatusOK, gin.H{
		"message":"submit",
		"path":ctx.Request.URL.Path,
	})
}

func readEndpoint(ctx *gin.Context)  {
	fmt.Println("read: ", ctx.Request.URL.Path)
	ctx.JSON(http.StatusOK, gin.H{
		"message":"read",
		"path":ctx.Request.URL.Path,
	})
}
