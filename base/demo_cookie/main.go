package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	// 路径参数
	r.POST("/user/save/:id", func(c *gin.Context) {
		c.Header("test", "headertest")
		c.JSON(http.StatusOK, c.Param("id"))

	})

	//文件参数
	r.POST("/user/file", func(c *gin.Context) {
		form, err := c.MultipartForm()
		if err != nil {
			fmt.Println("=======err", err)
		}
		files := form.File
		for _, fileArray := range files{
			for _, v := range fileArray{
				c.SaveUploadedFile(v, "./" + v.Filename)
			}
		}

		c.JSON(http.StatusOK, gin.H{
			"filename": form.Value,
			"len_file": len(files),

		})

	})

	// yaml
	r.GET("/user/yaml", func(c *gin.Context) {
		c.YAML(200, gin.H{"name": "ms", "age":19})

	})  // age: 19
		//name: ms

	// cookie
	r.GET("/cookie", func(c *gin.Context) {
		// 设置cookie
		c.SetCookie("site_cookie", "cookieValue", 3600, "/", "localhost", false, true)
		c.JSON(200, gin.H{
			"msg": "setting msg success",
		})

	})

	// read cookie
	r.GET("/read", func(c *gin.Context) {
		data, err := c.Cookie("site_cookie")
		if err != nil {
			c.String(http.StatusNotFound, "not found")
			return
		}
		c.String(200, "found data: " + data)
	})

	// delete cookie
	r.GET("/del", func(c *gin.Context) {
		// 设置cookie MaxAge设置为-1, 表示删除cookie
		c.SetCookie("site_cookie", "cookievalue", -1, "/", "localhost", false, true)
		c.String(http.StatusOK, "delete cookie")

	})



	r.Run(":8888")
}

// 路径参数