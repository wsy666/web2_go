package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func M1() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("m1 View Function Before")
		c.Next()
		fmt.Println("m1 View Function After")
	}
}

func M2() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("m2 View Function Before")
		c.Next()
		fmt.Println("m2 View Function After")
	}
}

func main() {
	r := gin.Default()
	r.Use(M1())
	r.Use(M2())
	r.GET("/", func(c *gin.Context) {
		fmt.Println("hello yuan")
		_, _ = c.Writer.WriteString("Hello World")
	})

	r.Run(":8888")
}
