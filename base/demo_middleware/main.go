package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		c.Set("example", "123456")

		c.Next()

		latency := time.Since(t)
		fmt.Println("latency: ", latency)

		status := c.Writer.Status()
		fmt.Println("status: ", status)

	}
}
func main() {
	r := gin.Default()
	r.Use(Logger())
	r.GET("/test", func(c *gin.Context) {
		example := c.MustGet("example").(string)
		fmt.Println("mustGetExample: ", example)

	})

	r.Run(":8888")
}
