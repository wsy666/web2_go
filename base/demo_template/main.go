package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func loadTemplateHandler() gin.HandlerFunc  {
	return func(c *gin.Context) {
		fullpath := "路径: " + c.FullPath()
		c.HTML(http.StatusOK, "other.html", gin.H{
			"fullpath":fullpath,
		})
	}

}

/* ==================================================================================
这里没有通过
r.LoadHTMLGlob(pattern); 这里的pattern是相对路径吗?
   ==================================================================================
*/

func main() {
	r := gin.Default()
	r.LoadHTMLGlob(".image/*")
	// 加载静态资源
	r.Static("/image", "../")

	r.GET("/html", loadTemplateHandler())

	r.Run(":8888")
}
