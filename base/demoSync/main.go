package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"time"
)

//当在中间件或 handler 中启动新的 Goroutine 时，不能使用原始的上下文，必须使用只读副本。



func main() {
	r := gin.Default()
	r.GET("/long_async", func(c *gin.Context) {
		cCp := c.Copy()
		// 异步打印不需要等待
		go func() {
			time.Sleep(5 * time.Second)
			log.Print("Done in path " + cCp.Request.URL.Path)
		}()
	})

	r.GET("/long_sync", func(c *gin.Context) {
		time.Sleep(5 * time.Second)
		// 要卡在那里5秒
		log.Print("Done in path " + c.Request.URL.Path)
	})

	r.Run(":8888")
}
