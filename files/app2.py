from enum import unique
from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime 

app = Flask(__name__)

db =  SQLAlchemy(app)

import os 
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + os.path.join(basedir, "app.db")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique= True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140)) 
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    
if __name__ == '__main__':
    # print(basedir)
    # print(app.config.get('SQLALCHEMY_DATABASE_URI'))
    try:
        print('start-----------------')
        db.create_all()
        print('end----------------')
    except:
        print('error')
    pass

    print('start to add element......')
    u = User(username='wsy1', email='xx1@yy.com')
    u1 = User(username='lrf2', email='rr2@yy.com')
    try:
        db.session.add(u)
        db.session.add(u1)
        db.session.commit()
    except:
        print('error in to add.....')
    finally:
        db.session.close()
    
    