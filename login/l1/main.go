package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{"name":"yuan"})
}

func login(c *gin.Context)  {
	// 获取get数据
	user := c.Query("user")
	pwd := c.Query("pwd")
	c.DefaultQuery("user", "匿名用户")
	fmt.Println("Get user: ", user)
	fmt.Println("Get pwd: ", pwd)
	fmt.Println("RequestMethod: ", c.Request.Method)

	c.HTML(http.StatusOK, "login.html", nil)
}

// auth
func auth(c *gin.Context) {
	// get post data
	user := c.PostForm("user")
	pwd := c.PostForm("pwd")
	fmt.Println("Post user: ", user)
	fmt.Println("Post pwd: ", pwd)

	if user == "yuan" && pwd == "123"{
		c.String(http.StatusOK, "验证成功")
	}else{
		c.String(http.StatusOK, "验证失败")
	}
}

func not_found(c *gin.Context)  {
	c.HTML(http.StatusNotFound, "notfound.html", nil)

}



/* 
删除书籍
*/
func article_delete(c *gin.Context)  {
	// 获取动态路径参数
	delete_id := c.Param("delete_id")
	fmt.Println("删除书籍", delete_id)
	c.Redirect(http.StatusMovedPermanently, "/index")
}

func article_year_month(c *gin.Context)  {
	fmt.Println(c.Param("year"))
	fmt.Println(c.Param("month"))
	// 获取全路径, url: 协议:IP :端口:路径?get参数
	fmt.Println(c.FullPath())
}

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("./templates/*")
	r.GET("/index", index)
	r.GET("/login", login)
	r.POST("/auth", auth)

	// 设置 博客路由
	blog := r.Group("/blog")
	{
		blog.GET("/articles/:year/:month", article_year_month)
		blog.GET("/articles/delete/:delete_id", article_delete)
	}
		
	
	r.Run(":8888")
}


